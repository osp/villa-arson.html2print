Generating the doc
==================

    pip install -r requirements/dev.txt
    cd docs
    make html


* html2pdf
:   generic service to generate pdf from any html page

* publish
:   write css and add urls

<http://localhost:8000/html2pdf/generate/>
:   front-end to generate a pdf from any webpage



sudo systemctl restart rabbitmq
celery -A testproject worker -l info


@import url('http://meyerweb.com/eric/tools/css/reset/reset.css');
@import url('https://fonts.googleapis.com/css?family=Oswald');

:root {
    --page-width: 148.5mm;
    --page-height: 210mm;

    --margin-top: 30mm;
    --margin-right: 20mm;
    --margin-bottom: 30mm;
    --margin-left: 20mm;

    --column-count: 2;
    --column-gap: 9pt;
}




article {
    font-family: 'Oswald', sans-serif;
    font-size: 12pt;
    line-height: 1.25;
    color: brown;
}

article header {
    text-align: center;
    margin-bottom: 1.25em;
}

article .lead {
    text-align: left;
    font-size: 1.25em;

}

article .headline {
    font-size: 2em;
}

article h2 {
    margin-top: 1.25em;
    position: relative;
    top: -.25em;

}

article p + p {
    text-indent: 3em;
}



http://localhost:8000/static/samples/le-grand-robert.html
