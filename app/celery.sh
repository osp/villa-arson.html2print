#! /usr/bin/env bash

NAME="celery"                          # Name of the application
USER=www-data                             # the user to run as
GROUP=www-data                            # the group to run as
DJANGODIR=/srv/datadisk02/www/xyz.villa-arson.htmltopdf/app/testproject/app  # Django project directory

VENV=/srv/datadisk02/www/xyz.villa-arson.htmltopdf/venv

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source $VENV/bin/activate
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Start Celery
exec $VENV/bin/celery -A testproject worker -l info
