from django import forms
from .tasks import htmltopdf


class GenerateForm(forms.Form):
    url = forms.URLField()

    def print(self):
        url = self.cleaned_data.get("url")
        return htmltopdf.delay(url)
