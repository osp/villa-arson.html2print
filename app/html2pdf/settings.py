import os
from django.conf import settings


cmd = 'xvfb-run -- /srv/datadisk02/src/tools.ospkit/poc/headless_ospkit/OSPKitPDF "{}" "{}"'
HTML2PDF_CMD = getattr(settings, 'HTML2PDF_CMD', cmd)
HTML2PDF_DESTINATION_PATH = getattr(settings, 'HTML2PDF_DESTINATION_PATH', os.path.join(settings.MEDIA_ROOT, "pdf"))
