# Create your tasks here
from celery import shared_task
import shlex
import os
import subprocess
from html2pdf import settings
from uuid import uuid4


@shared_task
def htmltopdf(url, width=210, height=297):
    env = os.environ.copy()

    pdf_name = "{}.pdf".format(uuid4().hex)
    cmd = settings.HTML2PDF_CMD.format(url, pdf_name)
    destination_path = settings.HTML2PDF_DESTINATION_PATH

    p1 = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stdin=subprocess.PIPE, cwd=destination_path, env=env)
    (stdout, stderr) = p1.communicate()

    #  return os.path.join(destination_path, pdf_name)
    return pdf_name
