from django.conf.urls import url
from . import views


app_name = 'html2pdf'


task_pattern = r'(?P<task_id>[\w\d\-\.]+)'


urlpatterns = [
    url(r'^celery/%s/status/?$' % task_pattern, views.task_status, name='celery-task_status'),
    url(r'^generate/$', views.GenerateView.as_view(), name='generate'),
]
