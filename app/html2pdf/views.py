from django.http import HttpResponse, JsonResponse
from django.views import View
from django.views.generic.edit import FormView
from django.urls import reverse
from celery import states
from celery.result import AsyncResult
from celery.utils import get_full_cls_name
from celery.utils.encoding import safe_repr
from .forms import GenerateForm

import json


from django.views.decorators.csrf import csrf_exempt


#  class GenerateView(FormView):
#      template_name = 'html2pdf/generate.html'
#      form_class = GenerateForm

#      def get_form_kwargs(self):
#          """Return the keyword arguments for instantiating the form."""
#          kwargs = {
#              'initial': self.get_initial(),
#              'prefix': self.get_prefix(),
#          }

#          if self.request.method in ('POST', 'PUT'):
#              kwargs.update({
#                  'data': self.request.POST,
#                  'files': self.request.FILES,
#              })
#          return kwargs

#      @csrf_exempt
#      def dispatch(self, *args, **kwargs):
#          return super(GenerateView, self).dispatch(*args, **kwargs)

#      def form_valid(self, form):
#          # This method is called when valid form data has been POSTed.
#          # It should return an HttpResponse.
#          task_id = form.print()
#          return HttpResponse(task_id)


class GenerateView(View):
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(GenerateView, self).dispatch(*args, **kwargs)

    def post(self, request):
        jsonstring = request.body

        if type(jsonstring) is bytes:
            jsonstring = jsonstring.decode('utf-8')

        json_data = json.loads(jsonstring)
        form = GenerateForm(json_data)
        if form.is_valid():
            task = form.print()
            url = reverse('html2pdf:celery-task_status', kwargs={"task_id": task.task_id},)
            url = request.build_absolute_uri(url)
            return JsonResponse({"task_id": task.task_id, "task_url": url})
        else:
            context = {
                'status': '400', 'reason': 'bad request'
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 400
            return response


def task_status(request, task_id):
    """ Based on https://github.com/celery/django-celery/, licence BSD
    """
    result = AsyncResult(task_id)
    state, retval = result.state, result.result
    response_data = {'id': task_id, 'status': state, 'result': retval}

    if state in states.EXCEPTION_STATES:
        traceback = result.traceback
        response_data.update({
            'result': safe_repr(retval),
            'exc': get_full_cls_name(retval.__class__),
            'traceback': traceback
        })
    return JsonResponse({'task': response_data})
