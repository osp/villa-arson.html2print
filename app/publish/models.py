from django.db import models

import requests


class Article(models.Model):
    story = models.URLField()
    css = models.TextField(blank=True)
    story_cached = models.TextField(blank=True)

    # http://stackoverflow.com/questions/1355150/django-when-saving-how-can-you-check-if-a-field-has-changed#1793323
    __original_story = None

    def __init__(self, *args, **kwargs):
        super(Article, self).__init__(*args, **kwargs)
        self.__original_story = self.story

    #  def __unicode__(self):
    #      return self.title or "Sans titre"

    #  @models.permalink
    #  def get_absolute_url(self):
    #      return ('article-detail-site', (), {'slug': self.slug})

    def save(self, *args, **kwargs):
        if self.pk is None or self.story != self.__original_story:
            r = requests.get(self.story)
            r.encoding = 'utf-8' # Force UTF-8 in case the server is improperly configured
            #  self.story_cached = bleach.clean(r.text, strip=True)
            self.story_cached = r.text

        super(Article, self).save(*args, **kwargs) # Call the "real" save() method.
        self.__original_name = self.story
