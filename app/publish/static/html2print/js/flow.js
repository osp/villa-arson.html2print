;(function () {
  // Ensure same names for function swheteher we have webkit's native
  // implementation or throught polyfill
  if ((!document.getNamedFlows) && document.webkitGetNamedFlows) {
    document.getNamedFlows = document.webkitGetNamedFlows;
  }
  
  if (!document.getNamedFlow) {
    document.getNamedFlow = function (name) {
      return document.getNamedFlows(name).namedItem(name);
    }
  }

  'use strict';
  window.fitFlow = function (flow, template, container, isPageTest) {

    // Global parameters
    var batchSize = 25;
  
    function makeEvent(name) {
      var e = document.createEvent("Event");
      e.initEvent(name, false, true);
      return e;
    }

    function getPageForRegion (node) {
      while(!isPageTest(node)) {
        node = node.parentNode;
      }

      return node;
    }

    function addPagesIfNecessary () {
      console.log('adding pages');
      if (flow.overset == true) {
        for (var i=0;i<batchSize;i++) {
          container.appendChild(template.cloneNode(true));
        };
      } else {
        flow.dispatchEvent(makeEvent("regionlayoutextendingcomplete"));
      }
    }

    function tighten () {
      // Remove event listeners to avoid bug in webkit where it left one page too many
      flow.removeEventListener("webkitregionoversetchange", addPagesIfNecessary);
      flow.removeEventListener("regionoversetchange", addPagesIfNecessary);

      var regions = flow.getRegions(),
          lastPageWithContent = getPageForRegion(regions[(flow.firstEmptyRegionIndex - 1)]);

      while (lastPageWithContent.nextSibling) {
        lastPageWithContent.parentNode.removeChild(lastPageWithContent.nextSibling);
      }

      window.requestAnimationFrame(function () {
        flow.dispatchEvent(makeEvent("regionlayoutfittingcomplete"));
        flow.dispatchEvent(makeEvent("regionlayoutcomplete"));
      });

    }

    flow.dispatchEvent(makeEvent("regionlayoutstart"));
    
    if (flow.overset == true) {  
      flow.addEventListener("webkitregionoversetchange", addPagesIfNecessary, false);
      flow.addEventListener("regionoversetchange", addPagesIfNecessary, false);
      flow.addEventListener("regionlayoutextendingcomplete", tighten, false);

      addPagesIfNecessary();
    } else {
      flow.dispatchEvent(makeEvent("regionlayoutcomplete"));
    }
  };
})(window, document);
