;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';

    HTML2PDF.MainApp = new Backbone.Marionette.Application({
        region: 'body',

        onStart: function(options) {
            var router = new HTML2PDF.MainRouter({application: this});

            /** Starts the URL handling framework */
            Backbone.history.start({
                pushState: true,
                root: '/publish/sandbox/'
            });
        }
    });
})();
