;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';

    HTML2PDF.SandboxCollection = Backbone.Collection.extend({
        url: '/api/articles/',

        model: HTML2PDF.SandboxModel
    });
})();
