;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';

    HTML2PDF.MainController = Backbone.Marionette.Object.extend({
        sandboxList: function() {
            var app = this.getOption('application');
            // app.getRegion().empty();
            var myListView = new HTML2PDF.SandboxListView();

            app.showView(myListView);
        },
        sandboxEdit: function(id) {
            var app = this.getOption('application');
            // app.getRegion().empty();
            var sandboxModel = new HTML2PDF.SandboxModel({id: parseInt(id)});
            var myBaseView = new HTML2PDF.SandboxEditView({model: sandboxModel});

            app.showView(myBaseView);
        }
    });
})();
