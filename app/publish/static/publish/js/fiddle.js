;(function($, undefined) {
    'use strict';

    function get_status (task_id) {
        $.get('/html2pdf/celery/' + task_id + "/status/", $('form').serialize(), function(response) {
            if (response.task.status == "PENDING") {
                window.setTimeout(function() {
                    get_status(task_id);
                }, 500)
            } else {
                console.log(response)
                var $link = $("<a>").attr("href", "/media/pdf/" + response.task.result).text(response.task.result);
                $("body").append($link);
            }
        })
    }

    $('input[type="submit"]').on("click", function(event) {
        // $.post('/html2pdf/generate/', $('form').serialize(), function(response){
        $.post('/html2pdf/generate/', {url: 'http://lemonde.fr/'}, function(response) {
            get_status(response);
        })
    
        return false;
    });
})(Zepto);
