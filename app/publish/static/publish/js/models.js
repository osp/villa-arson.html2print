;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';

    HTML2PDF.SandboxModel = Backbone.Model.extend({
        defaults: {
            story: "http://localhost:8000/static/samples/le-grand-robert.html",
            css: ":root { --column-count: 2; }"
        },

        urlRoot: '/api/articles/',

        url: function() {
            var original_url = Backbone.Model.prototype.url.call(this);
            var parsed_url = original_url + (original_url.charAt(original_url.length - 1) == '/' ? '' : '/');

            return parsed_url;
        },
    });
})();
