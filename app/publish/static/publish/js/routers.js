;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';

    HTML2PDF.MainRouter = Backbone.Marionette.AppRouter.extend({
        appRoutes: {
            '': 'sandboxList',
            ':id(/)': 'sandboxEdit',
        },

        initialize(options) {
            this.controller = new HTML2PDF.MainController(options);
        },
    });
})();
