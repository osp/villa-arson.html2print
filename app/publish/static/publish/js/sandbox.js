window.AA = window.AA || {};


(function(undefined) {
    'use strict';

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    AA.ArticleModel = Backbone.Model.extend({
        urlRoot: "/api/articles/",
        url: function() {
            var original_url = Backbone.Model.prototype.url.call( this );
            var parsed_url = original_url + ( original_url.charAt( original_url.length - 1 ) == '/' ? '' : '/' );

            return parsed_url;
        }
    });

    AA.ArticleView = Backbone.View.extend({
        el: 'body',
        events: {
            "click .save" : "save",
            "click .generate" : "generate",
        },
        initialize: function() {
            Split(['#a', '#b'],
            {
                gutterSize: 8,
                cursor: 'col-resize'
            })

            Split(['#c', '#d'],
            {
                direction: 'vertical',
                sizes: [50, 50],
                gutterSize: 8,
                cursor: 'row-resize'
            })
            
            this.cssEditor = ace.edit("c");
            this.cssEditor.setTheme("ace/theme/monokai");
            this.cssEditor.getSession().setMode("ace/mode/css");

            this.htmlEditor = ace.edit("d");
            this.htmlEditor.setTheme("ace/theme/monokai");
            this.htmlEditor.getSession().setMode("ace/mode/html");
            
            this.listenTo(this.model, 'sync', this.setValue);
            // this.render();
        },
        setValue: function() {
            this.cssEditor.setValue(this.model.get("css"))
            this.htmlEditor.setValue(this.model.get("story"))
        },
        generate: function() {
            function get_status (task_id) {
                $.get('/html2pdf/celery/' + task_id + "/status/", $('form').serialize(), function(response) {
                    if (response.task.status == "PENDING") {
                        window.setTimeout(function() {
                            get_status(task_id);
                        }, 500)
                    } else {
                        console.log(response)
                        var $link = $("<a>").attr("href", "/media/pdf/" + response.task.result).text(response.task.result);
                        $(".pdf-link").html($link);
                    }
                })
            }

            $.post('/html2pdf/generate/', {url: "http://localhost:8000/static/samples/le-grand-robert.html"}, function(response) {
                get_status(response);
            })
        },
        save: function() {
            var that = this;

            this.model
                .save({
                        css: this.cssEditor.getValue(),
                        story: this.htmlEditor.getValue()
                    }, {
                    success: function(model, result, xhr) {
                        $("iframe").get(0).contentWindow.location.reload();
                    },
                    error: function(model, xhr, options) {
                    }
                });
        },
    });

})();  // end of the namespace AA


var model = new AA.ArticleModel({ id : 1 });
var view = new AA.ArticleView({ model: model });
view.model.fetch();

// vim: set foldmethod=indent:
