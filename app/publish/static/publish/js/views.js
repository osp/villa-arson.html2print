;
window.HTML2PDF = window.HTML2PDF || {};


;
(function(undefined) {
    'use strict';
    HTML2PDF.HtmlEditView = Backbone.Marionette.View.extend({
        template: '#sandbox-html-tpl',

        ui: {
            'field': '#html-field'
        },

        initialize: function(options) {
            this.options = options;

            this.listenTo(this.model, 'sync', this.setValue);
        },

        onRender: function(options) {
            this.getUI('field').val(this.model.get("story"));
        },

        setValue: function() {
            this.getUI('field').val(this.model.get("story"));
        },

        getValue: function() {
            return this.getUI('field').val();
        },
    });


    HTML2PDF.CssEditView = Backbone.Marionette.View.extend({
        template: '#sandbox-css-tpl',

        attributes: {
            class: 'editor-wrapper'
        },
        
        // ui: {
        //     'editor': '.editor'
        // },


        initialize: function(options) {
            this.options = options;

            this.listenTo(this.model, 'sync', this.setValue);
        },

        onRender: function(options) {
            // console.log(this.getUI('editor'));
            this.editor = ace.edit(this.el);
            this.editor.setTheme("ace/theme/monokai");
            this.editor.setOptions({
                fontSize: "11pt"
            });
            this.editor.getSession().setMode("ace/mode/css");
            this.editor.setValue(this.model.get("css"))
        },

        setValue: function() {
            this.editor.setValue(this.model.get("css"))
        },

        getValue: function() {
            return this.editor.getValue();
        }
    });


    HTML2PDF.SandboxEditView = Backbone.Marionette.View.extend({
        tagName: "div",

        attributes: {
            class: "main-wrapper"
        },

        template: '#sandbox-edit-tpl',

        regions: {
            html: '#html-field-wrapper',
            css: '#css'
        },

        triggers: {
            "click #save-button": "save",
            "click #generate-button": "generate",
        },

        onSave: function() {
            console.log("saving");
            var that = this;

            var cssEditor = this.getRegion("css").currentView;
            var htmlEditor = this.getRegion("html").currentView;

            this.model
                .save({
                    css: cssEditor.getValue(),
                    story: htmlEditor.getValue()
                }, {
                    success: function(model, result, xhr) {
                        var val = $('input[name=switch]:checked').val();
                        if (val == "HTML") {
                            $("iframe").get(0).contentWindow.location = "/publish/article/" + that.model.get("id");
                        } else if (val == "PDF") {
                            console.log("pdf");
                            $("#save-button").addClass("spin");
                            that.triggerMethod("generate");
                        }
                    },
                    error: function(model, xhr, options) {}
                });
        },

        onGenerate: function(view, event) {
            console.log("generate");

            function get_status (task_id) {
                $.get('/html2pdf/celery/' + task_id + "/status/", $('form').serialize(), function(response) {
                    if (response.task.status == "PENDING") {
                        window.setTimeout(function() {
                            get_status(task_id);
                        }, 500)
                    } else {
                        console.log(response)
                        var url = "/media/pdf/" + response.task.result;
                        var $link = $("<a>").attr("href", url).text(response.task.result);
                        $(".pdf-link").html($link);

                        $("iframe").get(0).contentWindow.location = url;
                        $("#save-button").removeClass("spin");
                    }
                })
            }

            var full = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

            $.ajax({
                url: "/html2pdf/generate/",
                method: "POST",
                data: JSON.stringify({ url: full + "/publish/article/" + this.model.id }), 
                contentType: 'application/json; charset=utf-8',
                processData: false,
                dataType: 'json',
                success: function(response) {
                    get_status(response.task_id);
                }
            });
        },

        onRender: function() {

            var myHtmlView = new HTML2PDF.HtmlEditView({
                model: this.model
            });
            var myCssView = new HTML2PDF.CssEditView({
                model: this.model
            });

            var that = this;

            this.model.fetch({
                success: function() {
                    console.log(that.model);
                    that.showChildView('html', myHtmlView);
                    that.showChildView('css', myCssView);
                }
            });

            this.$("iframe").get(0).src = "/publish/article/" + this.model.id;
        },

        onAttach: function() {
            var editor = this.$el.find("#editor").get(0);
            var preview = this.$el.find("#preview").get(0);

            Split([ editor, preview ], {
                gutterSize: 8,
                cursor: 'col-resize'
            })
        }
    });


    HTML2PDF.ListItemView = Backbone.Marionette.View.extend({
        tagName: 'li',
        template: '#list-item-tpl'
    });


    HTML2PDF.ListView = Backbone.Marionette.CollectionView.extend({
        tagName: 'ol',
        collection: HTML2PDF.SandboxCollection,
        childView: HTML2PDF.ListItemView,

        initialize: function(options) {
            this.listenTo(this.collection, 'sync', this.render);
        },
    });


    HTML2PDF.CreateView = Backbone.Marionette.View.extend({
        template: '#create-tpl',

        triggers: {
            'submit .create-form': 'create'
        },

        onCreate: function(event) {
            var story = this.$el.find('[name="story"]').val();
            var myModel = new HTML2PDF.SandboxModel();
            myModel.save({story: story}, {success: function() {

                Backbone.history.navigate('/' + myModel.id, {trigger: true});
            }});
        },
    });


    HTML2PDF.SandboxListView = Backbone.Marionette.View.extend({
        tagName: "div",

        attributes: {
            class: "main-wrapper"
        },

        template: '#sandbox-list-tpl',

        regions: {
            list: '#list',
            create: '#create'
        },

        onRender: function() {
            var sandboxCollection = new HTML2PDF.SandboxCollection();
            var myListView = new HTML2PDF.ListView({
                collection: sandboxCollection
            });
            var myCreateView = new HTML2PDF.CreateView();

            var that = this;

            myListView.collection.fetch({
                success: function() {
                    that.showChildView('list', myListView);
                    that.showChildView('create', myCreateView);
                }
            });
        }
    });
})();
