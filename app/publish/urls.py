from django.conf.urls import url
from . import views


app_name = 'publish'


urlpatterns = [
    url(r'^sandbox/', views.SandboxView.as_view(), name='sandbox'),
    url(r'^article/(?P<pk>\d+)/$', views.ArticleDetailView.as_view(), name='article-detail'),
]
