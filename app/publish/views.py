from django.views import generic
from django.views.generic.base import TemplateView

from rest_framework import viewsets

from .models import Article
from .serializers import ArticleSerializer


class ArticleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class ArticleDetailView(generic.DetailView):
    model = Article


class SandboxView(TemplateView):
    template_name = 'publish/sandbox.html'

    #  def get_context_data(self, *args, **kwargs):
    #      ctx = super(SandboxView, self).get_context_data(*args, **kwargs)
    #      ctx['slug'] = self.kwargs['slug'] # or Tag.objects.get(slug=...)
    #      return ctx
