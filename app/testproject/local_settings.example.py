# Local settings
# Customize as needed


LOCAL_SETTINGS = True
from testproject.settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = False


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'


# Django Celery with Rabbitmq
# CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_ALWAYS_EAGER = False
CELERY_RESULT_BACKEND = 'django-db'
