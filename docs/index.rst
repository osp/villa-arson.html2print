.. html2pdf documentation master file, created by
   sphinx-quickstart on Thu Jun 15 23:32:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to html2pdf's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Html2pdf is a Django app that allows for server-side PDF generation.


API
===

Html2pdf allows for programmatic creation of PDF over HTTP POST requests. The tool you choose is up to you. Here, we use `CUrl`.


Creating a PDF
--------------

Here is a one liner to create a PDF of a page:

.. code-block:: bash

    curl -H "Content-Type: application/json" -X POST -d '{"url": "http://lemonde.fr"}' http://localhost:8000/html2pdf/generate/

You can also store the json data in a file:

.. code-block:: bash

    curl -H "Content-Type: application/json" -X POST -d @myfile.json http://localhost:8000/html2pdf/generate/

The content of the file mwfile.json would be:

.. code-block:: json

    {"url": "http://lemonde.fr"}

Since the generation is done asynchroniously to avoid blocking the server, the command will return the id of the task:

.. code-block:: json

    {"task_id": "f7fc2f69-2426-43a2-84a1-26a941e2bc0c"}


.. code-block:: bash

    url=$(curl -H "Content-Type: application/json" -X POST -d '{"url": "http://lemonde.fr"}' http://localhost:8000/html2pdf/generate/ | python2 -c "import sys, json; print json.load(sys.stdin)['task_url']")

Retrieving a PDF
----------------

xxx

Variables
===================
The fastest way to style a document in html2print is by setting css variables. They are used in html2pdf's stylesheet. You can set them in the css.

.. code-block:: css

    :root {
        --variable-name: value;
    }

Available css variables
-----------------------

Page geometry
~~~~~~~~~~~~~

By setting these variables you can define the size and margins of the html2pdf document.

.. glossary::

    ``--page-width``
        width of the page. Default: 210mm (A4)

    ``--page-height`` 
        height of the page. Default: 297mm (A4)

.. glossary::

    ``--margin-left``
        page margin left default 0

    ``--margin-top``
        page margin top default 0

    ``--margin-right``
        page margin right default 0

    ``--margin-bottom``
        page margin bottom default 0

.. glossary::

    ``--header-height``
        Height of the running header
        default 0

    ``--footer-height``
        Height of the footer
        default ``0``

    ``--header-align``
        Alignment of content within the running header. 
        default center

    ``--footer-align``
        Defines the alignment of content within the footer. 
        default center

    ``--header-content``
        Content that will be inserted into the running header

    ``--footer-content``
        Content that will be inserted into the footer inserted in the footer

Multi-column layout
~~~~~~~~~~~~~~~~~~~

Html2pdf provides a (basic) functionality to flow content into multicolumn layouts. By default all columns will have the same width. Through css it's possible to influence this behaviour.

.. glossary::

    ``--column-count``
        default 1
        Integer representing the amount of columns on the page. By default all columns have the same width.

    ``--column-gap``
        Whitespace between columns

Page numbering
~~~~~~~~~~~~~~

Using css counters it's possible to generate page numbers. html2pdf provides the page-number counter to do so. This varible holds a shortcut to that counter.

.. glossary::

    ``--page-number``
        Shortcut to the css counter, containing current page number. Use it for example in ``--footer-content`` to insert page numbers on the bottom of each document.




Basic concepts
==============



.. code-block:: scss

    :root {
        --page-width: 148.5mm;
        --page-height: 210mm;

        --margin-top: 30mm;
        --margin-right: 20mm;
        --margin-bottom: 30mm;
        --margin-left: 20mm;

        --column-count: 2;
        --column-gap: 9pt;
    }

Fonts
-----

html2pdf does not (yet) support the uploading of fonts with a document, but you can pull in externally hosted fonts to style your document.

.. code-block:: css

    @import url('http://meyerweb.com/eric/tools/css/reset/reset.css');
    @import url('https://fonts.googleapis.com/css?family=Oswald');


HTML Structure
--------------

html2pdf inserts an html strucutre around your content to flow it in pages. All content will ultimately be flown into the .columns

.. code-block:: css

    #pages
        .paper
            .page
                .header
                .body
                    .column
                .footer

.. article {
..     font-family: 'Oswald', sans-serif;
..     font-size: 12pt;
..     line-height: 1.25;
..     color: brown;
.. }

.. article header {
..     text-align: center;
..     margin-bottom: 1.25em;
.. }

.. article .lead {
..     text-align: left;
..     font-size: 1.25em;

.. }

.. article .headline {
..     font-size: 2em;
.. }

.. article h2 {
..     margin-top: 1.25em;
..     position: relative;
..     top: -.25em;

.. }

.. article p + p {
..     text-indent: 3em;
.. }



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
