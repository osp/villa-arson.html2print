;(function(undefined) {
    'use strict';

    // http://stackoverflow.com/questions/8567114/how-to-make-an-ajax-call-without-jquery
    function callAjax(url, callback){
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        // xmlhttp.withCredentials = true;
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                // CHANGE WINDOW TITLE WITH FILENAME
                var windowTitle = /\/.+\/(.+).html/.exec(url);
                top.document.title = windowTitle[1];
                callback(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    // loads stories
    window.addEventListener("load", function () {
        var stories = document.querySelectorAll('article[data-src]');

        for (var i = 0, l = stories.length; i < l; i ++) {
            var v = stories[i],
                src = v.dataset.src,
                sc = 0,
                cb = v.dataset.cb;

            callAjax(src, function(data) {
                v.innerHTML = data;

                if (cb) {
                    eval(cb);
                }

                sc++;

                if (sc == stories.length) {
                    window.requestAnimationFrame(function () {
                        var e = document.createEvent('Event');
                        e.initEvent('storiesloaded', true, true);
                        document.dispatchEvent(e);
                    });
                }
            });
        }
    }, false);
})();

;(function(undefined) {
    'use strict';

    if (!(document.webkitGetNamedFlows || document.getNamedFlows)) {
        console.log('no support for css regions; loading the polyfill');
        var script = document.createElement('script');
        script.setAttribute('src', '../../js/css-regions-polyfill.min.js');        
        // script.setAttribute('src', '../../js/css-regions-polyfill-custom-medor.js');
        document.head.appendChild(script);
    } else {
        console.log('native support for CSS regions');
        document.nativesupport = true;
    }
})();
